import { useEffect, useState } from 'react';
import { RecoverySession, Session, TrainingSession } from '../../../data';
import { ReactComponent as ChevronLeft } from '../../../icons/chevron-left.svg';
import { ReactComponent as Volleybal } from '../../../icons/volleyball.svg';
import { ReactComponent as Battery } from '../../../icons/battery-charging.svg';
import { ReactComponent as Text } from '../../../icons/text.svg';

import { months } from '../../../utils/months';
import { timeConverter } from '../../../utils/timeconverter';

import {
  BatteryContainer,
  ChevronContainer,
  DetailViewContainer,
  Header,
  RecoveryDescription,
  RecoveryDescriptionContainer,
  Description,
  DescriptionContainer,
  SessionDetails,
  SessionInfo,
  TextIconContainer,
  VolleybalContainer,
} from './styled';
import { RestLevelIndicator } from '../Indicators/RestLevelIndicator';
import { IntensityLevelDescription, RestLevelDescription } from '../../../types';
import { IntensityLevelIndicator } from '../Indicators/IntensityLevelIndicator';

type Props = {
  detailViewSession: Session;
  setShowDetailView: React.Dispatch<React.SetStateAction<boolean>>;
};

export const DetailView = ({ detailViewSession, setShowDetailView }: Props) => {
  const [intensityLevelIndication, setIntensityLevelIndication] = useState<IntensityLevelDescription | ''>('');
  const [restLevelIndication, setRestLevelIndication] = useState<RestLevelDescription | ''>('');
  const [month, setMonth] = useState('');

  const recoverySession = detailViewSession as RecoverySession;
  const trainingSession = detailViewSession as TrainingSession;
  const date = detailViewSession.date.split('-');
  const dayOfTheMonth = date.at(-1)?.split('0').join('');

  useEffect(() => {
    months.forEach((month, index) => {
      if (index + 1 === parseInt(date.at(1)?.split('0').join('') as string)) {
        setMonth(month);
      }
    });
  }, [date]);

  const TrainingSession = () => {
    return (
      <>
        <DescriptionContainer>
          <RestLevelIndicator session={recoverySession} setRestLevelIndication={setRestLevelIndication} />
          <Description>
            I felt <strong>{restLevelIndication.toLocaleLowerCase()}</strong> before the training session.
          </Description>
        </DescriptionContainer>
        <DescriptionContainer>
          <IntensityLevelIndicator
            session={trainingSession}
            setIntensityLevelIndication={setIntensityLevelIndication}
          />
          <Description>
            I perceived the training as <strong>{intensityLevelIndication.toLocaleLowerCase()}</strong>. <br />
            The total load was <strong>{Math.round(trainingSession.load)}</strong>
          </Description>
        </DescriptionContainer>
      </>
    );
  };

  const RecoverySession = () => {
    return (
      <>
        <RecoveryDescriptionContainer>
          <TextIconContainer>
            <Text width={25} />
          </TextIconContainer>
          <RecoveryDescription>{recoverySession.description}</RecoveryDescription>
        </RecoveryDescriptionContainer>
        <DescriptionContainer>
          <RestLevelIndicator session={recoverySession} setRestLevelIndication={setRestLevelIndication} />
          <Description>
            I felt <strong>{restLevelIndication.toLocaleLowerCase()}</strong> after the recovery session.
          </Description>
        </DescriptionContainer>
      </>
    );
  };

  return (
    <DetailViewContainer data-testid="detail-view">
      <Header sessionType={detailViewSession.type}>
        <ChevronContainer onClick={() => setShowDetailView(false)}>
          <ChevronLeft width={75} />
        </ChevronContainer>
        {detailViewSession.type === 'training' ? (
          <VolleybalContainer>
            <Volleybal y={-25} />
          </VolleybalContainer>
        ) : (
          <BatteryContainer>
            <Battery y={-25} />
          </BatteryContainer>
        )}
      </Header>
      <SessionInfo sessionType={detailViewSession.type}>
        <h1>{detailViewSession.type}</h1>
        <h2>
          {month} {dayOfTheMonth?.includes('1') ? `${dayOfTheMonth}st` : `${dayOfTheMonth}th`}
        </h2>
        <h3>
          {detailViewSession.startTime} - {detailViewSession.endTime} • {timeConverter(detailViewSession.duration)}
        </h3>
      </SessionInfo>
      <SessionDetails>
        {detailViewSession.type === 'training' && <TrainingSession />}
        {detailViewSession.type === 'recovery' && <RecoverySession />}
      </SessionDetails>
    </DetailViewContainer>
  );
};
