import { Session } from '../../../data';
import { ReactComponent as Battery } from '../../../icons/battery-charging.svg';
import { useEffect, useState } from 'react';
import { RestLevelDescription, RestLevelIndicatorEnum } from '../../../types';
import { BatteryContainer } from './styled';

export const RestLevelIndicator = ({
  session,
  setRestLevelIndication,
}: {
  session: Session;
  setRestLevelIndication: React.Dispatch<React.SetStateAction<RestLevelDescription | ''>>;
}) => {
  const [restLevelColor, setRestLevelColor] = useState<string>();

  useEffect(() => {
    switch (session.restLevel) {
      case RestLevelIndicatorEnum.depleted: {
        setRestLevelColor('#EF4444');
        setRestLevelIndication('Depleted');
        break;
      }
      case RestLevelIndicatorEnum.ok: {
        setRestLevelColor('#FDE047');
        setRestLevelIndication('Ok');
        break;
      }
      case RestLevelIndicatorEnum.topFit: {
        setRestLevelColor('#22C55E');
        setRestLevelIndication('Top fit');
        break;
      }
      default:
        break;
    }
  }, [session, setRestLevelIndication]);

  return (
    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" width={25}>
      <g color={restLevelColor}>
        <circle cx="50" cy="50" r="50" fill="currentColor" />
      </g>
      <BatteryContainer level={session.restLevel}>
        <Battery x={10} y={10} width={75} height={75} />
      </BatteryContainer>
    </svg>
  );
};
