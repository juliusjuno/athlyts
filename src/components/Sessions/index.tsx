import { useEffect, useState } from 'react';
import { mockApiCallToFetchSessions, Session } from '../../data';
import { List } from './List';
import { InformationHeader } from './styled';

export const Sessions = () => {
  const [sessions, setSessions] = useState<Session[]>([]);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    (async () => {
      try {
        const _sessions = await mockApiCallToFetchSessions();
        setSessions(_sessions);
      } catch (error) {
        const msg = 'Error while fetching sessions, please refresh the page.'
        setErrorMessage(msg);
        console.error(error, { description: msg });
      }
    })()
  }, []);

  return (
    <>
      {sessions.length ? (
        <List sessions={sessions} />
      ) : (
        Boolean(!errorMessage.length) &&
        <InformationHeader>Loading sessions...</InformationHeader>
      )}

      {Boolean(errorMessage.length) && !sessions.length && <InformationHeader>{errorMessage}</InformationHeader>}
    </>
  );
};
