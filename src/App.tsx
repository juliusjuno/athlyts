import styled, { createGlobalStyle } from 'styled-components/macro';
import { Sessions } from './components/Sessions';

// Good reasoning for not using FC as a component type https://medium.com/raccoons-group/why-you-probably-shouldnt-use-react-fc-to-type-your-react-components-37ca1243dd13
export const App = () => {
  return (
    <>
      <GlobalStyle />
      <Container>
        <header>
          <h1>Athlyts session tracker</h1>
        </header>
        <main>
          <Sessions />
        </main>
        <footer>Footer</footer>
      </Container>
    </>
  );
};

const GlobalStyle = createGlobalStyle`
  :root {
    --athlyts-blue: #60a5fa;
    --athlyts-green: #4ade80;
    --athlyts-violet: #a78afb;
    --athlyts-yellow:#fbbf25;
    --athlyts-dark-green: #365314;
    --athlyts-dark-blue: #0C4A6E;
  }
`;

const Container = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  header {
    h1 {
      font-family: ethnocentric, sans-serif;
      color: white;
      padding: 15px;
    }
    background: black;
    height: 100%;
    box-shadow: -1px 4px 8px 3px #00000018;
  }
  main {
    font-family: transducer, sans-serif;
    height: 100%;
  }
  footer {
    background: black;
    height: 100%;
    margin-top: auto;
  }
`;
