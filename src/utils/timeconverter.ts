export function timeConverter(num: number) {
  const hours = num / 60;
  const roundedHours = Math.floor(hours);
  const minutes = (hours - roundedHours) * 60;
  const roundedminutes = Math.round(minutes);
  const isNotZero = roundedminutes !== 0;
  const result = `${roundedHours} ${roundedHours > 1 ? 'hours' : 'hour'} ${isNotZero ? 'and' : ''} ${
    isNotZero ? roundedminutes : ''
  } ${isNotZero ? (roundedminutes > 1 ? 'minutes' : 'minute') : ''}`;
  return result;
}
