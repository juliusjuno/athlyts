import styled from 'styled-components';
import { SessionType } from '../../../../types';

export const DetailViewContainer = styled.div``;

export const Header = styled.div<{ sessionType: SessionType }>`
  display: flex;
  justify-content: space-between;
  background: ${(props) =>
    props.sessionType === 'training' ? 'var(--athlyts-dark-green)' : 'var(--athlyts-dark-blue)'};
`;

export const ChevronContainer = styled.svg`
  fill: white;
  cursor: pointer;
`;

export const TextIconContainer = styled.div``;
export const RecoveryDescription = styled.div``;

export const RecoveryDescriptionContainer = styled.div`
  display: flex;
  ${RecoveryDescription} {
    margin-left: 10px;
  }
`;

export const SessionInfo = styled.div<{ sessionType: SessionType }>`
  background: ${(props) =>
    props.sessionType === 'training' ? 'var(--athlyts-dark-green)' : 'var(--athlyts-dark-blue)'};
  color: ${(props) => (props.sessionType === 'training' ? '#A3E635' : '#38BDF8')};
  height: 10rem;
  h1 {
    text-transform: capitalize;
    margin-left: 1rem;
    margin-block-start: 0;
    margin-block-end: 0;
  }
  h2 {
    margin-left: 1rem;
    margin-block-start: 0;
    margin-block-end: 0;
  }
  h3 {
    margin-left: 1rem;
    margin-block-start: 0;
    margin-block-end: 0;
  }
`;
export const SessionDetails = styled.div`
  margin: 1rem;
`;

export const VolleybalContainer = styled.svg`
  fill: #4e7c10;
`;

export const BatteryContainer = styled.svg`
  fill: #0269a0;
`;

export const Description = styled.div``;

export const DescriptionContainer = styled.div`
  display: flex;
  margin: 25px 0;
  ${Description} {
    margin-left: 10px;
  }
`;
