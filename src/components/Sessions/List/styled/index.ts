import styled, { css } from 'styled-components/macro';
import { SessionType } from '../../../../types';

export const ListContainer = styled.div`
  ul {
    height: 85vh;
    margin: auto 0;
  }
  overflow-y: auto;
`;

export const ListItemActions = styled.div`
  display: flex;
  align-items: center;
  border-radius: 5px;
`;

export const ListItem = styled.li<{ isFavorite: boolean; sessionType: SessionType }>`
  display: flex;
  justify-content: space-between;
  width: 75%;
  list-style: none;
  border-radius: 5px;
  margin: 1rem 0;
  padding: 0.5rem;
  box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
  ${(props) =>
    props.sessionType === 'training' &&
    css`
      border: 2px solid var(--athlyts-blue);
      &:hover {
        background-color: var(--athlyts-blue);
        transition: 0.4s ease-in all;
      }
    `};

  ${(props) =>
    props.sessionType === 'recovery' &&
    css`
      border: 2px solid var(--athlyts-yellow);
      &:hover {
        background-color: var(--athlyts-yellow);
        transition: 0.4s ease-in all;
      }
    `};

  ${ListItemActions} {
    ${(props) =>
      props.isFavorite &&
      css`
        border: var(--athlyts-green) 15px solid;
        background: var(--athlyts-green);
      `};
  }
`;

export const Button = styled.button<{ primary?: boolean }>`
  background: white;
  border-radius: 3px;
  border: 2px solid var(--athlyts-violet);
  cursor: pointer;
  color: var(--athlyts-violet);
  font-family: transducer, sans-serif;
  height: 50%;
  margin: 0 1em;
  padding: 0.25em 1em;
  text-align: center;
  &:hover {
    box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
    transition: 0.2s ease-in all;
  }
  &:active {
    box-shadow: none;
  }
  ${(props) =>
    props.primary &&
    css`
      background: var(--athlyts-violet);
      color: white;
      svg {
        fill: white;
      }
      &:hover {
        svg {
          animation: beating 2s ease-in-out infinite normal;
        }
      }
    `}

  @keyframes beating {
    0%,
    50%,
    100% {
      transform: scale(1, 1);
    }
    30%,
    80% {
      transform: scale(0.62, 0.65);
    }
  }
`;

export const SessionInfo = styled.div`
  display: flex;
  flex-direction: column;
  h4{
    font-weight: inherit;
    strong {
      text-transform:capitalize;
    }
  }
}
`;
