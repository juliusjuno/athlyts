import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { List } from './components/Sessions/List';
import { mockApiCallToFetchSessions } from './data';
import { timeConverter } from './utils/timeconverter';

test('Displays a list of training and recovery sessions', async () => {
  try {
    const _sessions = await mockApiCallToFetchSessions();
    render(<List sessions={_sessions} />);
    const ListContainer = screen.getByTestId('list-container');
    const listItems = screen.getAllByTestId('list-item') as HTMLElement[];
    expect(ListContainer).toBeInTheDocument();
    expect(listItems).toHaveLength(_sessions.length);
  } catch (error) {
    const newErr = new Error('timeout');
    // eslint-disable-next-line jest/no-conditional-expect
    expect(error).toStrictEqual(newErr);
  }
});

test('Provides minimal information on a session', async () => {
  try {
    const _sessions = await mockApiCallToFetchSessions();
    render(<List sessions={_sessions} />);
    const ListContainer = screen.getByTestId('list-container');
    const timeOfFirstListItem = timeConverter(_sessions[0].duration);
    const FirstListItem = screen.getAllByText(`Session duration: ${timeOfFirstListItem}`).at(0) as HTMLElement;
    expect(ListContainer).toContainElement(FirstListItem);
  } catch (error) {
    const newErr = new Error('timeout');
    // eslint-disable-next-line jest/no-conditional-expect
    expect(error).toStrictEqual(newErr);
  }
});

test('Can mark certain rows as favorites and sorts favorites to the top of the list', async () => {
  try {
    const _sessions = await mockApiCallToFetchSessions();
    render(<List sessions={_sessions} />);
    const ListContainer = screen.getByTestId('list-container');
    const timeOfFirstListItem = timeConverter(_sessions[0].duration);
    const lastListItem = screen.getAllByText(`Session duration: ${timeOfFirstListItem}`).at(-1) as HTMLElement;
    const lastFavoriteButton = within(ListContainer).getAllByTestId('favorite').at(-1) as HTMLElement;
    userEvent.click(lastFavoriteButton);
    expect(screen.getByTestId('unfavorite-icon')).toBeInTheDocument()
    const firstItem = screen.getAllByText(`Session duration: ${timeOfFirstListItem}`).at(0) as HTMLElement;
    expect(lastListItem).toEqual(firstItem)
  } catch (error) {
    const newErr = new Error('timeout');
    // eslint-disable-next-line jest/no-conditional-expect
    expect(error).toStrictEqual(newErr);
  }
});


test('Can open a session in a separate screen, displaying details of the selected session(a detail screen)', async () => {
  try {
    const _sessions = await mockApiCallToFetchSessions();
    render(<List sessions={_sessions} />);
    const ListContainer = screen.getByTestId('list-container');
    const firstItemsDetailButton = within(ListContainer).getAllByText('View session details').at(0) as HTMLElement;
    userEvent.click(firstItemsDetailButton);
    const detailView = screen.getByTestId('detail-view');
    const header = within(detailView).getByText(_sessions[0].type);
    expect(detailView).toContainElement(header);
  } catch (error) {
    const newErr = new Error('timeout');
    // eslint-disable-next-line jest/no-conditional-expect
    expect(error).toStrictEqual(newErr);
  }
});
