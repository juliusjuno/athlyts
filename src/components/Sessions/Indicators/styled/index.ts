import styled, { css } from 'styled-components';
import { IntensityLevel, RestLevel } from '../../../../data';
import { IntensityLevelIndicatorEnum, RestLevelIndicatorEnum } from '../../../../types';

export const IndicatorsContainer = styled.div`
  display: flex;
`;

export const IndicatorContainer = styled.div`
  display: flex;
  align-items: center;
  span {
    margin-left: 10px;
  }
`;

export const RestLevelChip = styled.span<{ restLevelindication: RestLevel }>`
  padding: 1px 5px;
  border-radius: 5px;
  ${(props) => {
    switch (props.restLevelindication) {
      case RestLevelIndicatorEnum.depleted:
        return css`
          background: #ef4444;
          color: white;
        `;
      case RestLevelIndicatorEnum.ok:
        return css`
          background: #fde047;
          color: #14532d;
        `;
      case RestLevelIndicatorEnum.topFit:
        return css`
          background: #22c55e;
          color: #14532d;
        `;

      default:
        return '';
    }
  }};
`;

export const BatteryContainer = styled.svg<{ level: RestLevel }>`
      fill: ${(props) => {
        switch (props.level) {
          case RestLevelIndicatorEnum.depleted:
            return 'white';

          default:
            return '#14532D';
        }
      }};

  }
`;

export const IntensityLevelChip = styled.span<{ intensityLevelindication: IntensityLevel }>`
  padding: 1px 5px;
  border-radius: 5px;
  ${(props) => {
    switch (props.intensityLevelindication) {
      case IntensityLevelIndicatorEnum.easy:
        return css`
          background: #e7e5e4;
          color: #57534e;
        `;
      case IntensityLevelIndicatorEnum.intense:
        return css`
          background: #78716c;
          color: white;
        `;
      case IntensityLevelIndicatorEnum.extreme:
        return css`
          background: #1c1917;
          color: white;
        `;

      default:
        return '';
    }
  }};
`;

export const SpeedOMeterContainer = styled.svg<{ level: IntensityLevel }>`
      fill: ${(props) => {
        switch (props.level) {
          case IntensityLevelIndicatorEnum.easy:
            return '#57534E';

          default:
            return 'white';
        }
      }};

  }
`;

export const VR = styled.div`
  margin: 0 10px;
  border-left: 2px solid rgba(0, 0, 0, 0.3);
  height: 2rem;
`;
