import { Session, TrainingSession } from '../../../data';
import { Button, ListItem, ListItemActions, SessionInfo } from './styled';
import { ReactComponent as Heart } from '../../../icons/heart.svg';
import { ReactComponent as HeartOutline } from '../../../icons/heart-outline.svg';
import { useEffect, useState } from 'react';
import { timeConverter } from '../../../utils/timeconverter';
import { RestLevelIndicator } from '../Indicators/RestLevelIndicator';
import { RestLevelChip, IndicatorContainer, IndicatorsContainer, VR, IntensityLevelChip } from '../Indicators/styled';
import { IntensityLevelDescription, RestLevelDescription } from '../../../types';
import { IntensityLevelIndicator } from '../Indicators/IntensityLevelIndicator';
import { months } from '../../../utils/months';

export const ListItemComponent = ({
    session,
    favorites,
    handleOpenDetailView,
    handleNewFavorite,
}: {
    session: Session;
    favorites: Session[];
    handleOpenDetailView: (id: number) => void;
    handleNewFavorite: (session: Session) => void;
}) => {
    const [restLevelIndication, setRestLevelIndication] = useState<RestLevelDescription | ''>('');
    const [intensityLevelIndication, setIntensityLevelIndication] = useState<IntensityLevelDescription | ''>('');
    const trainingSession = session as TrainingSession;
    const [month, setMonth] = useState('');

    const date = session.date.split('-');

    useEffect(() => {
        months.forEach((month, index) => {
            if (index + 1 === parseInt(date.at(1)?.split('0').join('') as string)) {
                setMonth(month);
            }
        });
    }, [date]);

    return (
        <ListItem isFavorite={favorites.includes(session)} sessionType={session.type} data-testid="list-item">
            <SessionInfo>
                <h4>
                    <strong>
                        <span>{session.type}</span> session
                    </strong>
                    {month} {date.at(-1)?.split('0').join('')} {session.date.split('-').at(0)}
                </h4>
                <IndicatorsContainer>
                    <IndicatorContainer>
                        <RestLevelIndicator session={session} setRestLevelIndication={setRestLevelIndication} />
                        <RestLevelChip restLevelindication={session.restLevel}>{restLevelIndication}</RestLevelChip>
                    </IndicatorContainer>
                    {session.type === 'training' && (
                        <IndicatorContainer>
                            <VR />{' '}
                            <IntensityLevelIndicator session={session} setIntensityLevelIndication={setIntensityLevelIndication} />
                            <IntensityLevelChip intensityLevelindication={trainingSession.intensityLevel}>
                                {intensityLevelIndication}
                            </IntensityLevelChip>
                        </IndicatorContainer>
                    )}
                </IndicatorsContainer>
                <span>Session duration: {timeConverter(session.duration)}</span>
            </SessionInfo>
            <ListItemActions>
                <Button onClick={() => handleOpenDetailView(session.id)}> View session details</Button>
                <Button onClick={() => handleNewFavorite(session)} primary data-testid="favorite">
                    {favorites.includes(session) ? <HeartOutline width={25} data-testid="unfavorite-icon" /> : <Heart width={25} />}
                </Button>
            </ListItemActions>
        </ListItem>
    );
};
