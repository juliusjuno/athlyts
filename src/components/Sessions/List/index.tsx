import { useState } from 'react';
import { Session } from '../../../data';
import { DetailView } from '../DetailView';
import { ListItemComponent } from './ListItem';
import { ListContainer } from './styled';

type Props = {
  sessions: Session[];
};

export const List = ({ sessions }: Props) => {
  const [showDetailView, setShowDetailView] = useState(false);
  const [detailViewSession, setDetailViewSession] = useState<Session>();
  const [favorites, setFavorites] = useState<Session[]>([]);

  const handleOpenDetailView = (session: Session) => {
    setShowDetailView(true);
    setDetailViewSession(session);
  };

  const handleNewFavorite = (session: Session) => {
    favorites.includes(session)
      ? setFavorites(favorites.filter((favorite) => favorite.id !== session.id))
      : setFavorites([session, ...favorites]);
  };

  return (
    <ListContainer data-testid="list-container">
      {sessions && !showDetailView ? (
        <ul>
          {favorites.map((favorite) => {
            return (
              <ListItemComponent
                key={favorite.id}
                session={favorite}
                favorites={favorites}
                handleOpenDetailView={() => handleOpenDetailView(favorite)}
                handleNewFavorite={handleNewFavorite}
              />
            );
          })}
          {sessions.map((session) => {
            return (
              !favorites.includes(session) && (
                <ListItemComponent
                  key={session.id}
                  session={session}
                  favorites={favorites}
                  handleOpenDetailView={() => handleOpenDetailView(session)}
                  handleNewFavorite={handleNewFavorite}
                />
              )
            );
          })}
        </ul>
      ) : (
        <DetailView detailViewSession={detailViewSession as Session} setShowDetailView={setShowDetailView} />
      )}
    </ListContainer>
  );
};
