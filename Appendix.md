## Steps and Thought Process

- Started by extracting out requirements into tests

- Created the Sessions component
- Fetch mock-sessions
- Pass sessions to the List component

- Iterate over sessions
- Add styled-components for composability and reusability
- Create button to open detailview for training/recovery session
- Create detail view for both types of sessions
  - Add time converter util for session duration
  - Create rest and intensity indicators
- Add rest and intensity indicators to list item component
- Add session duration to list item
- Expand list item styling
- Add favorite button

  - Keep track of favorites in component-level state
  - Hide session from sessions list if it exsist in favorites array
  - Iterate and show favorites on top of sessions list
    - <code><strong>At this point I thought it might have been better to add an 'isFavorite' boolean flag to each
      session at the component-level state and reposition the item at the start of the array based on the flag's
      truthy/falsey value but decided against the refactor because of time constraints </strong></code>

- Finishing styling touches (such as hover states favorite button and background color list item)
- Refine tests + refactor obvious code smells
- Add Loading and error state

## Improvements that come to mind

- Keep track of sessions in a useReducer state so the app becomes more scalable
- Extract out resuable/generic UI components from component level styles
- Refactor components for better separation of concerns
