export type SessionType = 'training' | 'recovery';

export enum RestLevelIndicatorEnum {
  depleted = 1,
  ok = 2,
  topFit = 3,
}
export type RestLevelDescription = 'Depleted' | 'Ok' | 'Top fit';
export type IntensityLevelDescription = 'Easy' | 'Intense' | 'Extreme';

export enum IntensityLevelIndicatorEnum {
  easy = 1,
  intense = 2,
  extreme = 3,
}
