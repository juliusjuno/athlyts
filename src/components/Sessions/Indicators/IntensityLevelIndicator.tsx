import { Session, TrainingSession } from '../../../data';
import { ReactComponent as SpeedOMeter } from '../../../icons/speedometer.svg';
import { useEffect, useState } from 'react';
import { IntensityLevelDescription, IntensityLevelIndicatorEnum } from '../../../types';
import { SpeedOMeterContainer } from './styled';

export const IntensityLevelIndicator = ({
  session,
  setIntensityLevelIndication,
}: {
  session: Session;
  setIntensityLevelIndication: React.Dispatch<React.SetStateAction<IntensityLevelDescription | ''>>;
}) => {
  const trainingSession = session as TrainingSession;
  const [intensityLevelColor, setIntensityLevelColor] = useState<string>();

  useEffect(() => {
    switch (trainingSession.intensityLevel) {
      case IntensityLevelIndicatorEnum.easy: {
        setIntensityLevelColor('#E7E5E4');
        setIntensityLevelIndication('Easy');
        break;
      }
      case IntensityLevelIndicatorEnum.intense: {
        setIntensityLevelColor('#78716C');
        setIntensityLevelIndication('Intense');
        break;
      }
      case IntensityLevelIndicatorEnum.extreme: {
        setIntensityLevelColor('#1C1917');
        setIntensityLevelIndication('Extreme');
        break;
      }
      default:
        break;
    }
  }, [setIntensityLevelIndication, trainingSession]);

  return (
    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" width={25}>
      <g color={intensityLevelColor}>
        <circle cx="50" cy="50" r="50" fill="currentColor" />
      </g>
      <SpeedOMeterContainer level={trainingSession.intensityLevel}>
        <SpeedOMeter x={13} y={13} width={75} height={75}></SpeedOMeter>
      </SpeedOMeterContainer>
    </svg>
  );
};
